package com.example.abdulhafeez.easychat_hafeez;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
public class GetFirebaseRef {

    private static FirebaseDatabase mFirebaseDatabase;
    private  static FirebaseStorage mFirebaseStorage;

    public static FirebaseDatabase GetDbIns(){
        if (mFirebaseDatabase ==null){
            mFirebaseDatabase = FirebaseDatabase.getInstance();
            mFirebaseDatabase.setPersistenceEnabled(true);
            return mFirebaseDatabase;
        }
        return mFirebaseDatabase;
    }


}
