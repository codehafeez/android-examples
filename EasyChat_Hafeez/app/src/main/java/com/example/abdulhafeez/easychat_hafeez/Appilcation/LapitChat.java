package com.example.abdulhafeez.easychat_hafeez.Appilcation;
import android.app.Application;

import com.example.abdulhafeez.easychat_hafeez.GetFirebaseRef;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
public class LapitChat extends Application {

    private DatabaseReference mUserDatabase;
    private FirebaseAuth mAuth;
    @Override
    public void onCreate() {
        super.onCreate();
//        new Instabug.Builder(this, "bd796240086a293ce9ffb057bbb19fe8")
//                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
//                .build();

        mAuth = FirebaseAuth.getInstance();
        mUserDatabase = GetFirebaseRef.GetDbIns()
                .getReference()
                .child("Users")
                .child(mAuth.getCurrentUser().getUid());
        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot!=null){ mUserDatabase.child("online").onDisconnect().setValue(ServerValue.TIMESTAMP); }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }
}
